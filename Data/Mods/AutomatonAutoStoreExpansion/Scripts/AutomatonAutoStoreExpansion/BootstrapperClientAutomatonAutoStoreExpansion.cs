﻿using AtomicTorch.CBND.GameApi.Scripting;
using CryoFall.Automaton.Features;

namespace CryoFall.Automaton
{
    class BootstrapperClientAutomatonAutoStoreExpansion : BaseBootstrapper
    {
        public override void ClientInitialize()
        {
            AutomatonManager.AddFeature(FeatureAutoStore.Instance);
        }

    }
}
