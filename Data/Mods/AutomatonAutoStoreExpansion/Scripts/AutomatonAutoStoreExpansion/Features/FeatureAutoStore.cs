﻿namespace CryoFall.Automaton.Features
{
    using System.Collections.Generic;
    using System.Linq;
    using AtomicTorch.CBND.CoreMod;
    using AtomicTorch.CBND.CoreMod.StaticObjects;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Loot;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Crates;
    using AtomicTorch.CBND.CoreMod.Systems;
    using AtomicTorch.CBND.CoreMod.Systems.InteractionChecker;
    using AtomicTorch.CBND.CoreMod.Systems.Notifications;
    using AtomicTorch.CBND.CoreMod.Systems.Resources;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Managers;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Game.WorldObjects;
    using AtomicTorch.CBND.GameApi.Data;
    using AtomicTorch.CBND.GameApi.Data.State;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.CBND.GameApi.Scripting.ClientComponents;

    public class FeatureAutoStore : ProtoFeatureWithInteractionQueue<FeatureAutoStore>
    {
        public override string Name => "AutoStore";

        public override string Description => "Match up Items automatically";

        private IWorldObject openedContainer = null;

        private List<IWorldObject> containers = new List<IWorldObject>();

        private double timer = 0;

        private bool readyForInteraction = true;

        private IActionState lastActionState = null;

        protected override void PrepareFeature(List<IProtoEntity> entityList, List<IProtoEntity> requiredItemList)
        {
            entityList.AddRange(Api.FindProtoEntities<IProtoObjectCrate>());
        }

        protected override bool TestObject(IStaticWorldObject staticWorldObject)
        {
            return staticWorldObject.ProtoGameObject is IProtoObjectCrate protoCrate &&
                   protoCrate.SharedCanInteract(CurrentCharacter, staticWorldObject, false);
        }

        /// <summary>
        /// Stop everything.
        /// </summary>
        public override void Stop()
        {

            if (interactionQueue?.Count > 0)
            {
                if (interactionQueue[0].ProtoGameObject is IProtoObjectCrate protoCrate)
                {
                    protoCrate.ClientInteractFinish(interactionQueue[0]);
                }
                interactionQueue.Clear();
                InteractionCheckerSystem.CancelCurrentInteraction(CurrentCharacter);
            }
            readyForInteraction = true;
            lastActionState = null;
            openedContainer = null;
        }

        /// <summary>
        /// Setup any of subscriptions
        /// </summary>
        public override void SetupSubscriptions(ClientComponent parentComponent)
        {
            base.SetupSubscriptions(parentComponent);

            PrivateState.ClientSubscribe(
                s => s.CurrentActionState,
                OnActionStateChanged,
                parentComponent);
        }

        /// <summary>
        /// Init on component enabled.
        /// </summary>
        public override void Start(ClientComponent parentComponent)
        {
            base.Start(parentComponent);

            // Check if there an action in progress.
            if (PrivateState.CurrentActionState != null)
            {
                readyForInteraction = false;
                lastActionState = PrivateState.CurrentActionState;
            }

            // Check if we opened loot container before enabling component.
            var currentInteractionObject = InteractionCheckerSystem.SharedGetCurrentInteraction(CurrentCharacter);
            if (currentInteractionObject?.ProtoWorldObject is ProtoObjectLootContainer)
            {
                readyForInteraction = false;
                openedContainer = currentInteractionObject;
            }
        }

        private void OnActionStateChanged()
        {
            
            
        }

        protected override void CheckInteractionQueue()
        {
            timer += 1;
            if (timer > 100)
            {
                containers = new List<IWorldObject>();
                //FillInteractionQueue();
                timer = 0;
            }
            if (openedContainer != null)
            {
                if (InteractionCheckerSystem.SharedHasInteraction(CurrentCharacter, openedContainer, true))
                {
                    if (ClientCurrentInteractionMenu.currentMenuWindow is WindowCrateContainer crateContainer)
                    {
                        crateContainer.viewModel.ViewModelItemsContainerExchange.ExecuteCommandMatch(true);
                    }
                }
                else if (openedContainer.ProtoWorldObject
                                            .SharedCanInteract(CurrentCharacter, openedContainer, false))
                {
                    // Waiting for container private state from server.
                    return;
                }
                if (interactionQueue[0].ProtoGameObject is IProtoObjectCrate protoCrate)
                {
                    protoCrate.ClientInteractFinish(interactionQueue[0]);
                    InteractionCheckerSystem.CancelCurrentInteraction(CurrentCharacter);
                }
                openedContainer = null;
                readyForInteraction = true;
            }

            if (!readyForInteraction)
            {
                return;
            }
            
            // Remove from queue while it have object and they in our whitelist if:
            //  - object is destroyed
            //  - if object is container that we already have looted
            //  - if object not IProtoObjectGatherable
            //  - if we can not interact with object right now
            //  - if we can not gather anything from object
            while (interactionQueue.Count != 0 && EnabledEntityList.Contains(interactionQueue[0].ProtoGameObject) &&
                   (interactionQueue[0].IsDestroyed ||
                    !(interactionQueue[0].ProtoGameObject is IProtoObjectCrate protoCrate) ||
                    !protoCrate.SharedCanInteract(CurrentCharacter, interactionQueue[0], false) || containers.Contains(interactionQueue[0])))
            {
                interactionQueue.RemoveAt(0);
            }

            if (interactionQueue.Count == 0)
            {
                return;
            }
            
            if (interactionQueue[0].ProtoGameObject is IProtoObjectCrate protoCrate2)
            {
                protoCrate2.ClientInteractStart(interactionQueue[0]);
                openedContainer = interactionQueue[0];
                containers.Add(interactionQueue[0]);
                readyForInteraction = false;

            }
            //GatheringSystem.Instance.SharedStartAction(request);
        }
    }
}
