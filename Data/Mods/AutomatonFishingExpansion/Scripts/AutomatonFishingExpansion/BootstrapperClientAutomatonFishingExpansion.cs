﻿namespace CryoFall.Automaton
{
    using AtomicTorch.CBND.GameApi.Scripting;
	using CryoFall.Automaton.Features;

    public class BootstrapperClientAutomatonFishingExpansion : BaseBootstrapper
    {
        public override void ClientInitialize()
        {
            AutomatonManager.AddFeature(FeatureAutoFish.Instance);
        }
    }
}