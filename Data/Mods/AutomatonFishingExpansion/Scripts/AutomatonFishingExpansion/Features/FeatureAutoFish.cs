﻿namespace CryoFall.Automaton.Features
{
    using System.Collections.Generic;
    using System.Linq;
    using AtomicTorch.CBND.CoreMod.Items.Fishing;
    using AtomicTorch.CBND.CoreMod.Tiles;
    using AtomicTorch.CBND.GameApi.Data;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.CBND.CoreMod.Systems.FishingSystem;
    using CryoFall.Automaton.ClientSettings;

    public class FeatureAutoFish : ProtoFeature<FeatureAutoFish>
    {
        public override string Name => "AutoFishing";

        public override string Description => "Automatically fish in nearby water if Fishing rod is equipped.(Mouse needs to be over the water)";

        private List<IProtoEntity> PermittedTiles = new List<IProtoEntity>();

        private bool waitingForServer = false;

        protected override void PrepareFeature(List<IProtoEntity> entityList, List<IProtoEntity> requiredItemList)
        {
            PermittedTiles.AddRange(new List<IProtoEntity>(Api.FindProtoEntities<TileWaterLake>()));
            PermittedTiles.AddRange(new List<IProtoEntity>(Api.FindProtoEntities<TileWaterSea>()));
            requiredItemList.AddRange(Api.FindProtoEntities<ItemFishingRod>());
        }

        public override void PrepareOptions(SettingsFeature settingsFeature)
        {
            AddOptionIsEnabled(settingsFeature);
        }

        public override void Execute()
        {
            TryToStartFishAction();
            TryCatchFish();
        }

        private void TryToStartFishAction()
        {
            if (IsEnabled && CheckPrecondition() && IsWaterNearby() && !waitingForServer &&
                PrivateState.CurrentActionState == null)
            {
                FishingSystem.Instance.ClientTryStartAction();
            }
        }

        private void TryCatchFish()
        {
            if (PrivateState.CurrentActionState is FishingActionState fishingActionState)
            {
                if (!(fishingActionState.SharedFishingSession is null)
                && FishingSession.GetPublicState(fishingActionState.SharedFishingSession)
                                 .IsFishBiting)
                {
                    FishingSystem.ClientPullFish(fishingActionState.SharedFishingSession);
                    FishingSystem.Instance.ClientTryAbortAction();
                }
            }
        }

        private bool IsWaterNearby()
        {
            var tile = CurrentCharacter.Tile;
            if (PermittedTiles.Contains(tile.ProtoTile))
            {
                return true;
            }

            if (tile.EightNeighborTiles.Select(t => t.ProtoTile).Intersect(PermittedTiles).Any())
            {
                return true;
            }

            return false;
        }
    }
}
