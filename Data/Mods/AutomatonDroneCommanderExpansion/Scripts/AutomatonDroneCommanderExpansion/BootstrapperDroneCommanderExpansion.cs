﻿using AtomicTorch.CBND.GameApi.Scripting;
using CryoFall.Automaton.Features;

namespace CryoFall.Automaton
{
    class BootstrapperDroneCommanderExpansion : BaseBootstrapper
    {
        public override void ClientInitialize()
        {
            AutomatonManager.AddFeature(FeatureDroneCommander.Instance);
        }

    }
}
