﻿namespace CryoFall.Automaton.Features
{
    using System.Collections.Generic;
    using AtomicTorch.CBND.CoreMod.Items.Drones;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Minerals;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Vegetation.Trees;
    using AtomicTorch.CBND.CoreMod.Systems.CharacterDroneControl;
    using AtomicTorch.CBND.GameApi.Data;
    using AtomicTorch.CBND.GameApi.Data.Items;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.GameEngine.Common.Primitives;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Vegetation;
    using AtomicTorch.CBND.CoreMod.Systems.WorldObjectClaim;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Extensions;
    using System.Linq;
    using AtomicTorch.CBND.CoreMod.Helpers.Client;
    using AtomicTorch.CBND.CoreMod.Systems.ItemDurability;
    using CryoFall.Automaton.ClientSettings.Options;
    using CryoFall.Automaton.ClientSettings;

    public class FeatureDroneCommander : ProtoFeature<FeatureDroneCommander>
    {
        public override string Name => "DroneCommander";

        public override string Description => "Automatically commands drones to harvest selected ressources";

        private const double TreeGrowthProgressThreshold = 0.75;

        static double MinimumItemDurability = 0.01;

        protected override void PrepareFeature(List<IProtoEntity> entityList, List<IProtoEntity> requiredItemList)
        {
            entityList.AddRange(Api.FindProtoEntities<IProtoObjectMineral>());
            entityList.RemoveAll(t => !(((IProtoObjectMineral)t).IsAllowDroneMining));
            entityList.AddRange(Api.FindProtoEntities<IProtoObjectTree>());
            requiredItemList.AddRange(Api.FindProtoEntities<IProtoItemDroneControl>());
        }

        public override void PrepareOptions(SettingsFeature settingsFeature)
        {
            AddOptionIsEnabled(settingsFeature);
            Options.Add(new OptionSeparator());
            AddOptionEntityList(settingsFeature);
            Options.Add(new OptionSeparator());
            AddOptionDurability(settingsFeature);
        }

        private void AddOptionDurability(SettingsFeature settingsFeature)
        {
            Options.Add(new OptionTextBox<double>(
                parentSettings: settingsFeature,
                id: "Durability Threshold",
                label: "Use Items until: (%)",
                defaultValue: 0d,
                valueChangedCallback: (val) => FeatureDroneCommander.MinimumItemDurability = val/100,
                toolTip: "Defines when DroneCommander stops using the items, so they wont get destroyed."));
        }

        public override void Execute()
        {
            if (!(IsEnabled && CheckPrecondition()) || !(SelectedItem.ProtoItem is IProtoItemDroneControl protoItemControl) 
                || ItemDurabilitySystem.SharedGetDurabilityValue(SelectedItem) < protoItemControl.DurabilityMax*MinimumItemDurability)
            {
                return;
            }
            TrySendDrone();

        }

        public Vector2Ushort GetClosestEntity()
        {
            IStaticWorldObject selectedWorldObject = null;
            var selectedDistanceSqr = long.MaxValue;
            using var tempExceptTargets = Api.Shared.GetTempList<Vector2Ushort>();

            foreach (IProtoWorldObject entity in EnabledEntityList)
            {
                if (entity is null)
                {
                    return default;
                }
                var objectsNearby = Api.Client.World.GetStaticWorldObjectsOfProto(
                    (IProtoStaticWorldObject)entity);

                foreach (var worldObject in objectsNearby)
                {
                    var position = worldObject.TilePosition;
                    var distanceSqr = position.TileSqrDistanceTo(CurrentCharacter.TilePosition);
                    if (distanceSqr >= selectedDistanceSqr)
                    {
                        // already found a closer candidate
                        continue;
                    }

                    if (!WorldObjectClaimSystem.SharedIsAllowInteraction(CurrentCharacter,
                                                                         worldObject,
                                                                         showClientNotification: false))
                    {
                        continue;
                    }

                    if (worldObject.ProtoGameObject is IProtoObjectVegetation protoObjectVegetation
                        && protoObjectVegetation.SharedGetGrowthProgress(worldObject) < TreeGrowthProgressThreshold)
                    {
                        // not a full grown vegetation, ignore for targeting
                        continue;
                    }

                    if (CharacterDroneControlSystem.SharedIsTargetAlreadyScheduledForAnyActiveDrone(
                            CurrentCharacter,
                            position,
                            logError: false)
                        || tempExceptTargets.AsList().Contains(position))
                    {
                        continue;
                    }

                    selectedWorldObject = worldObject;
                    selectedDistanceSqr = distanceSqr;
                }
            }
            return selectedWorldObject?.TilePosition ?? default;
        }

        public IItem ClientSelectNextDrone(List<IItem> exceptItems)
        {
            IItem selectedItem = null;
            var selectedItemOrder = uint.MinValue;
            var privateState = ClientCurrentCharacterHelper.PrivateState;

            // find a drone item with the highest remaining durability
            foreach (var item in privateState.ContainerHotbar.Items)
            {
                if (IsValidDroneItem(item, out var durability)
                    && durability >= selectedItemOrder)
                {
                    selectedItem = item;
                    selectedItemOrder = durability;
                }
            }

            foreach (var item in privateState.ContainerInventory.Items)
            {
                if (IsValidDroneItem(item, out var durability)
                    && durability >= selectedItemOrder)
                {
                    selectedItem = item;
                    selectedItemOrder = durability;
                }
            }

            return selectedItem;

            bool IsValidDroneItem(IItem item, out uint durability)
            {
                if (item.ProtoItem is IProtoItemDrone protoItemDrone
                    && ItemDurabilitySystem.SharedGetDurabilityValue(item) > protoItemDrone.DurabilityMax*MinimumItemDurability
                    && !exceptItems.Contains(item))
                {
                    durability = ItemDurabilitySystem.SharedGetDurabilityValue(item);
                    return true;
                }

                durability = uint.MinValue;
                return false;
            }
        }

        private void TrySendDrone()
        {
            using var tempExceptDrones = Api.Shared.GetTempList<IItem>();
            using var tempExceptTargets = Api.Shared.GetTempList<Vector2Ushort>();

            Vector2Ushort targetPosition = GetClosestEntity();
            var targetObject = CharacterDroneControlSystem
                    .SharedGetCompatibleTarget(CurrentCharacter,
                                               targetPosition,
                                               out var hasIncompatibleTarget,
                                               out var isPveActionForbidden);

            var itemDrone = ClientSelectNextDrone(tempExceptDrones.AsList());

            if (itemDrone is null)
            {
                // no drone to select
                return;
            }

            tempExceptDrones.Add(itemDrone);

            if (targetPosition == default)
            {
                // no targets
                return;
            }


            if (!CharacterDroneControlSystem.ClientTryStartDrone(itemDrone,
                                                                 targetPosition,
                                                                 showErrorNotification: false))
            {
                return;
            }

            tempExceptTargets.Add(targetPosition);

            return;
        }
    }

}
