﻿namespace CryoFall.Automaton
{
    using AtomicTorch.CBND.GameApi.Scripting;
	using CryoFall.Automaton.Features;

    public class BootstrapperAutoShoot : BaseBootstrapper
    {
        public override void ClientInitialize()
        {
            AutomatonManager.AddFeature(FeatureAutoShoot.Instance);
        }
    }
}