﻿namespace CryoFall.Automaton.Features
{
    using System.Collections.Generic;
    using AtomicTorch.CBND.GameApi.Data;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.CBND.CoreMod.Systems.WorldObjectClaim;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Extensions;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.ConstructionSite;
    using AtomicTorch.CBND.CoreMod.Items.Tools.Toolboxes;
    using AtomicTorch.CBND.CoreMod.Systems.InteractionChecker;
    using AtomicTorch.CBND.CoreMod.Systems.Construction;
    using CryoFall.Automaton.ClientSettings;

    class FeatureAutoBuild : ProtoFeature<FeatureAutoBuild>
    {
        public override string Name => "AutoBuild";

        public override string Description => "Automatically builds nearby constructions sites if toolbox is equipped";

        private IStaticWorldObject lastIneractedObject = null;
        protected override void PrepareFeature(List<IProtoEntity> entityList, List<IProtoEntity> requiredItemList)
        {
            entityList.AddRange(Api.FindProtoEntities<ProtoObjectConstructionSite>());
            requiredItemList.AddRange(Api.FindProtoEntities<ProtoItemToolToolbox>());
        }

        public override void Execute()
        {
            if (!(IsEnabled && CheckPrecondition()))
            {
                return;
            }
            TryBuilding();

        }
        public override void PrepareOptions(SettingsFeature settingsFeature)
        {
            AddOptionIsEnabled(settingsFeature);
        }

        public override void Stop()
        {
            if (lastIneractedObject == null)
            {
                return;
            }
            ConstructionSystem.SharedAbortAction(CurrentCharacter, lastIneractedObject);
        }

        public IStaticWorldObject GetClosestEntity()
        {
            IStaticWorldObject selectedWorldObject = null;
            var selectedDistanceSqr = long.MaxValue;

            foreach (IProtoWorldObject entity in EntityList)
            {
                if (entity is null)
                {
                    return default;
                }
                var objectsNearby = Api.Client.World.GetStaticWorldObjectsOfProto(
                    (IProtoStaticWorldObject)entity);

                foreach (var worldObject in objectsNearby)
                {
                    var position = worldObject.TilePosition;
                    var distanceSqr = position.TileSqrDistanceTo(CurrentCharacter.TilePosition);
                    if (distanceSqr >= selectedDistanceSqr)
                    {
                        // already found a closer candidate
                        continue;
                    }

                    if (!WorldObjectClaimSystem.SharedIsAllowInteraction(CurrentCharacter,
                                                                         worldObject,
                                                                         showClientNotification: false))
                    {
                        continue;
                    }

                    selectedWorldObject = worldObject;
                    selectedDistanceSqr = distanceSqr;
                }
            }
            return selectedWorldObject;
        }

        private void TryBuilding()
        {
            IStaticWorldObject targetObject = GetClosestEntity();
            if (targetObject == null)
            {
                return;
            }
            if(ConstructionSystem.SharedCheckCanInteract(CurrentCharacter, targetObject, false) 
                && InteractionCheckerSystem.SharedHasInteraction(CurrentCharacter, targetObject, false) ==false)
            {
                ConstructionSystem.SharedStartAction(CurrentCharacter, targetObject);
                lastIneractedObject = targetObject;
            }
        }
    }

}
