﻿namespace CryoFall.Automaton
{
    using AtomicTorch.CBND.GameApi.Scripting;
    using CryoFall.Automaton.Features;

    public class BootstrapperAutoBuildExpansion : BaseBootstrapper
    {
        public override void ClientInitialize()
        {
            AutomatonManager.AddFeature(FeatureAutoBuild.Instance);
        }
    }
}