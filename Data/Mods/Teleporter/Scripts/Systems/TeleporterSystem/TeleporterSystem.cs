﻿namespace AtomicTorch.CBND.CoreMod.Systems.TeleporterSystem
{
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc;
    using AtomicTorch.CBND.CoreMod.Systems.Notifications;
    using AtomicTorch.CBND.CoreMod.Systems.Physics;
    using AtomicTorch.CBND.CoreMod.Systems.WorldObjectOwners;
    using AtomicTorch.CBND.GameApi.Data.Characters;
    using AtomicTorch.CBND.GameApi.Data.Physics;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Scripting.Network;
    using AtomicTorch.CBND.GameApi.ServicesServer;
    using AtomicTorch.GameEngine.Common.Primitives;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class TeleporterSystem : ProtoSystem<TeleporterSystem>
    {
        private static readonly IWorldServerService ServerWorldService = IsServer ? Server.World : null;

        public override string Name => "Teleporter system";

        public static Task<IReadOnlyList<TeleporterEntry>> ClientGetCharacterTeleportersAsync()
        {
            return Instance.CallServer(_ => _.ServerRemote_GetTeleporterList());
        }

        private IReadOnlyList<TeleporterEntry> ServerRemote_GetTeleporterList()
        {
            var character = ServerRemoteContext.Character;
            return ServerGetCharacterTeleporters(character);
        }

        public static List<TeleporterEntry> ServerGetCharacterTeleporters(
            ICharacter character)
        {
            var result = new List<TeleporterEntry>();
            var allTeleporters = Server.World.GetWorldObjectsOfProto<IProtoTeleporter>();
            // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
            foreach (IStaticWorldObject teleporter in allTeleporters)
            {
                result.Add(new TeleporterEntry(teleporter,
                                                      teleporter.PhysicsBody.Position + ((IProtoTeleporter)teleporter.ProtoGameObject).PlatformCenterWorldOffset));
            }
            return result;
        }
        public static Task<bool> ClientCallTeleportAsync(Vector2D worldPosition)
        {
            return Instance.CallServer(_ => _.ServerRemote_Teleport(worldPosition));
        }

        public bool ServerRemote_Teleport(Vector2D worldPosition)
        {
            var character = ServerRemoteContext.Character;
            ServerTeleport(character, worldPosition);
            return true;
        }

        private static void ServerTeleport(ICharacter player, Vector2D toPosition)
        {
            ServerWorldService.SetPosition(player, toPosition);
        }
    }
}
