﻿namespace AtomicTorch.CBND.CoreMod.Systems.TeleporterSystem
{
    using System.ComponentModel;
    using AtomicTorch.CBND.CoreMod.UI;

    public enum TeleporterStatus : byte
    {
        [Description(CoreStrings.VehicleGarage_VehicleStatus_InWorld)]
        Built = 0,
    }
}