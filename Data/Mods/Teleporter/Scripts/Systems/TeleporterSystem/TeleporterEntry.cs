﻿namespace AtomicTorch.CBND.CoreMod.Systems.TeleporterSystem
{
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc;
    using AtomicTorch.CBND.CoreMod.Systems.Physics;
    using AtomicTorch.CBND.GameApi.Data.Physics;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Scripting.Network;
    using AtomicTorch.GameEngine.Common.Primitives;
    using System;
    using System.Linq;

    public readonly struct TeleporterEntry : IRemoteCallParameter
    {
        public readonly uint Id;

        public readonly IProtoTeleporter ProtoTeleporter;

        public readonly Vector2D position;

        public TeleporterEntry(IWorldObject teleporter, Vector2D position)
        {
            this.Id = teleporter.Id;
            this.ProtoTeleporter = (IProtoTeleporter)teleporter.ProtoGameObject;
            this.position = position;
        }
    }
}