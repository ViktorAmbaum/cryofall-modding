﻿namespace AtomicTorch.CBND.CoreMod.Technologies.Tier5.Construction
{
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.CraftingStations;
    using AtomicTorch.CBND.CoreMod.CraftRecipes;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc;

    public class TechNodeRepairWorkbench : TechNode<TechGroupConstructionT5>
    {
        protected override void PrepareTechNode(Config config)
        {
            config.Effects
                  .AddStructure<ObjectTeleporter>();

            //config.SetRequiredNode<TechNodeCrowbar>();
        }
    }
}