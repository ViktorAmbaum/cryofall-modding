﻿namespace AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc
{
    public class ObjectTeleporterPrivateState : StructurePrivateState
    {
        public float darkMatterAmount { get; set; }

        public float DarkMatterCapacity { get; set; } = 500f;
        public ObjectTeleporterPrivateState()
        {
            darkMatterAmount = 50f;
        }
    }
}
