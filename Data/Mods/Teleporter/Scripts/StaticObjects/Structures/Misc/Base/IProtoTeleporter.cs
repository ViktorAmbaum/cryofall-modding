﻿namespace AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc
{
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.GameEngine.Common.DataStructures;
    using AtomicTorch.GameEngine.Common.Primitives;

    public interface IProtoTeleporter : IProtoObjectStructure, IInteractableProtoWorldObject
    {
        Vector2D PlatformCenterWorldOffset { get; }

        public float DarkMatterAmount { get; set; }

        public float DarkMatterCapacity { get; set; }

        void SharedGetVehiclesOnPlatform(
            IStaticWorldObject vehicleAssemblyBay,
            ITempList<IDynamicWorldObject> result);

        bool SharedIsBaySpaceBlocked(IStaticWorldObject vehicleAssemblyBay);
    }
}
