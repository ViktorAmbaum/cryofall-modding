﻿namespace AtomicTorch.CBND.CoreMod.UI
{
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class TeleporterModStrings
    {
        public const string WindowObjectTeleporter_TabTeleportationChamber = "Teleportation Chamber";

        public const string WindowObjectTeleporter_TabDarkMatterReservoir = "DarkMatter Reservoir";

        public const string WindowObjectTeleporter_TeleporterList = "Teleporter stations";

        public const string TeleportationChamber_TitleAccessibleTeleporters = "You have access to these teleporters:";

        public const string TeleportationChamber_TitleNoAccessibleTeleporters = "You have no access to other teleporters.";

        public const string TeleportationChamber_ButtonTeleportToSelectedStation = "Start teleportation";

        public const string DarkMatterReservoir_ButtonCreateDarkMatter = "Create DarkMatter";
    }
}