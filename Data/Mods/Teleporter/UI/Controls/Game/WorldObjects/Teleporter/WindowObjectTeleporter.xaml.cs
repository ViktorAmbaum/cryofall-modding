﻿namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.WorldObjects.Teleporter
{
    using System.Collections.Generic;
    using System.Linq;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc;
    using AtomicTorch.CBND.CoreMod.Systems.Notifications;
    using AtomicTorch.CBND.CoreMod.Systems.TeleporterSystem;
    using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Core;
    using AtomicTorch.CBND.CoreMod.Vehicles;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Game.Map;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Game.Map.Data;

    public partial class WindowObjectTeleporter : BaseUserControlWithWindow
    {
        private static WindowObjectTeleporter instance;

        private IStaticWorldObject teleporter;

        public ViewModelWindowObjectTeleporter ViewModel { get; private set; }

        public static void CloseActiveMenu()
        {
            instance?.CloseWindow();
        }

        public static WindowObjectTeleporter Open(IStaticWorldObject teleporter)
        {
            if (instance != null
                && instance.teleporter == teleporter)
            {
                return instance;
            }

            var window = new WindowObjectTeleporter();
            instance = window;
            window.teleporter = teleporter;
            Api.Client.UI.LayoutRootChildren.Add(window);
            return instance;
        }

        protected override void InitControlWithWindow()
        {
            // TODO: redone this to cached window when NoesisGUI implement proper Storyboard.Completed triggers
            this.Window.IsCached = false;
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();

            var protoVehicles = ProtoVehicleHelper.AllVehicles
                                                  .ToList();
            var recipesCountTotal = protoVehicles.Count;
            this.RemoveLockedVehicles(protoVehicles);

            this.DataContext = this.ViewModel = new ViewModelWindowObjectTeleporter(
                                   this.teleporter,
                                   protoVehicles,
                                   recipesCountTotal);
        }

        protected override void OnUnloaded()
        {
            base.OnUnloaded();

            this.DataContext = null;
            this.ViewModel.Dispose();
            this.ViewModel = null;
            instance = null;
        }

        private void RemoveLockedVehicles(List<IProtoVehicle> list)
        {
            var character = Api.Client.Characters.CurrentPlayerCharacter;
            list.RemoveAll(r => !r.SharedIsTechUnlocked(character));
        }
    }
}
