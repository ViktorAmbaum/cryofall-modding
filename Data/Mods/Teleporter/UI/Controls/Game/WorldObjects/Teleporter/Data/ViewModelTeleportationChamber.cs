﻿namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.WorldObjects.Teleporter
{
    using AtomicTorch.CBND.CoreMod.ConsoleCommands.Player;
    using AtomicTorch.CBND.CoreMod.Helpers.Client;
    using AtomicTorch.CBND.CoreMod.Systems.Notifications;
    using AtomicTorch.CBND.CoreMod.Systems.TeleporterSystem;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Core;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Core.Data;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Resources;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class ViewModelTeleportationChamber : BaseViewModel
    {
        private readonly IStaticWorldObject teleporter;

        private ViewModelTeleporterEntry selectedTeleporter;

        public ObservableCollection<ViewModelTeleporterEntry> AccessibleTeleporter { get; }
            = new SuperObservableCollection<ViewModelTeleporterEntry>();

        public BaseCommand CommandTeleportToStation
            => new ActionCommand(this.ExcecuteCommandTeleportToStation);

        public bool IsGarageAvailable { get; private set; } = true;

        public bool CanTeleportToStation { get; private set; } = true;

        public ViewModelTeleportationChamber(IStaticWorldObject teleporter)
        {
            this.teleporter = teleporter;
            this.Refresh();
        }

        private async void Refresh()
        {
            if (this.IsDisposed)
            {
                return;
            }

            ClientTimersSystem.AddAction(delaySeconds: 0.5,
                                         this.Refresh);
            var currentTeleporters = await TeleporterSystem.ClientGetCharacterTeleportersAsync();

            if (this.IsDisposed)
            {
                return;
            }

            this.ApplyCurrentTeleporters(currentTeleporters);
    }

        private void ApplyCurrentTeleporters(IReadOnlyList<TeleporterEntry> currentTeleporters)
        {

            // update status or create view models for current vehicles
            foreach (var entry in currentTeleporters)
            {
                var isFound = false;
                foreach (var viewModel in this.AccessibleTeleporter)
                {
                    if (viewModel.TeleporterGameObjectId != entry.Id)
                    {
                        continue;
                    }

                    isFound = true;
                    break;
                }

                if (!isFound)
                {
                    this.AccessibleTeleporter.Add(
                        new ViewModelTeleporterEntry(entry.Id, entry.ProtoTeleporter, entry.position));
                }
            }

            // remove all view models which are not existing in the current vehicles list
            for (var index = 0; index < this.AccessibleTeleporter.Count; index++)
            {
                var viewModel = this.AccessibleTeleporter[index];
                var isFoundInCurrentEntries = false;
                foreach (var entry in currentTeleporters)
                {
                    if (entry.Id == this.teleporter.Id)
                    {
                        continue;
                    }
                    if (viewModel.TeleporterGameObjectId != entry.Id)
                    {
                        continue;
                    }

                    isFoundInCurrentEntries = true;
                    break;
                }

                if (isFoundInCurrentEntries)
                {
                    continue;
                }

                // this view model has no corresponding vehicle entry anymore
                this.AccessibleTeleporter.RemoveAt(index--);
                viewModel.Dispose();
            }
        }

        public ViewModelTeleporterEntry SelectedTeleporter
        {
            get => this.selectedTeleporter;
            set
            {
                if (this.selectedTeleporter == value)
                {
                    return;
                }

                this.selectedTeleporter = value;
                this.NotifyThisPropertyChanged();
            }
        }

        private void ExcecuteCommandTeleportToStation()
        {
            var entry = this.SelectedTeleporter;
            if (entry == null)
            {
                return;
            }
            TeleporterSystem.ClientCallTeleportAsync(entry.position);
            NotificationSystem.ClientShowNotification("Teleported to:" + entry.position);
        }

    }
}
