﻿namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.WorldObjects.Teleporter
{
    using AtomicTorch.CBND.CoreMod.Helpers.Client;
    using AtomicTorch.CBND.CoreMod.Items.Generic;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc;
    using AtomicTorch.CBND.CoreMod.Systems;
    using AtomicTorch.CBND.CoreMod.Systems.Notifications;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Core;
    using AtomicTorch.CBND.GameApi.Data.Items;
    using AtomicTorch.CBND.GameApi.Data.World;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ViewModelDarkMatterReservoir : BaseViewModel
    {
        public readonly IProtoTeleporter Teleporter;

        private ObjectTeleporterPrivateState teleporterPrivateState;

        public IReadOnlyList<ProtoItemWithCount> BuildRequiredItems => SetRequirements();

        public BaseCommand CommandCreateDarkMatter
            => new ActionCommand(this.ExcecuteCommandCreateDarkMatter);

        public float DarkMatterAmount { get; private set; }

        public float DarkMatterCapacity { get; private set; } = 500f;

        private float DarkMatterCreationAmount = 50f;

        public bool IsCanCreate { get; private set; }

        public ViewModelDarkMatterReservoir(IStaticWorldObject teleporter)
        {
            this.Teleporter = (IProtoTeleporter)teleporter.ProtoGameObject;
            this.DarkMatterAmount = Teleporter.DarkMatterAmount;

            if (this.DarkMatterAmount + this.DarkMatterCreationAmount < this.DarkMatterCapacity){
                IsCanCreate = true;
            }else
            {
                IsCanCreate = false;
            }
            this.UpdateIsCanCreate();
            this.SubscribeToContainersEvents();
        }

        private IReadOnlyList<ProtoItemWithCount> SetRequirements()
        {
            InputItems buildRequiredItems = new InputItems();
            buildRequiredItems.Add<ItemIngotSteel>(5);
            buildRequiredItems.Add<ItemIngotSteel>(5);
            buildRequiredItems.Add<ItemIngotSteel>(5);
            buildRequiredItems.Add<ItemIngotSteel>(5);
            return buildRequiredItems.AsReadOnly();
        }

        private void ExcecuteCommandCreateDarkMatter()
        {
            Teleporter.DarkMatterAmount += 50;
        }
        private void ContainersItemsResetHandler()
        {
            this.UpdateIsCanCreate();
        }

        private void ItemAddedOrRemovedOrCountChangedHandler(IItem item)
        {
            this.UpdateIsCanCreate();
        }

        private void SubscribeToContainersEvents()
        {
            ClientCurrentCharacterContainersHelper.ContainersItemsReset += this.ContainersItemsResetHandler;
            ClientCurrentCharacterContainersHelper.ItemAddedOrRemovedOrCountChanged +=
                this.ItemAddedOrRemovedOrCountChangedHandler;
        }

        private void UnsubscribeFromContainersEvents()
        {
            ClientCurrentCharacterContainersHelper.ContainersItemsReset -= this.ContainersItemsResetHandler;
            ClientCurrentCharacterContainersHelper.ItemAddedOrRemovedOrCountChanged -=
                this.ItemAddedOrRemovedOrCountChangedHandler;
        }

        private void UpdateIsCanCreate()
        {
            var currentPlayerCharacter = Api.Client.Characters.CurrentPlayerCharacter;
            var checkResult = false;
        }

        protected override void DisposeViewModel()
        {
            base.DisposeViewModel();

            if (this.Teleporter != null)
            {
                this.UnsubscribeFromContainersEvents();
            }
        }
    }
}
