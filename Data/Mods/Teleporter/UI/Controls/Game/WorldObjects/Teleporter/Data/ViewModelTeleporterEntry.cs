﻿namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.WorldObjects.Teleporter
{
    using AtomicTorch.CBND.CoreMod.Helpers.Client;
    using AtomicTorch.CBND.CoreMod.StaticObjects.Structures.Misc;
    using AtomicTorch.CBND.CoreMod.Systems.TeleporterSystem;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Core;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Game.Map;
    using AtomicTorch.CBND.GameApi.Resources;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
    using AtomicTorch.GameEngine.Common.Extensions;
    using AtomicTorch.GameEngine.Common.Primitives;
    using System.Windows;

    public class ViewModelTeleporterEntry : BaseViewModel
    {
        public ViewModelTeleporterEntry(
            uint teleporterGameObjectId,
            IProtoTeleporter protoTeleporter,
            Vector2D position)
        {
            this.TeleporterGameObjectId = teleporterGameObjectId;
            this.ProtoTeleporter = protoTeleporter;
            this.position = position;
        }
        public TextureBrush Icon => Api.Client.UI.GetTextureBrush(this.ProtoTeleporter.Icon);

        public TextureBrush MapIcon => Api.Client.UI.GetTextureBrush(this.CreateIcon());

        public IProtoTeleporter ProtoTeleporter { get; }

        public Vector2D position;

        public string Title => this.ProtoTeleporter.Name;

        public uint TeleporterGameObjectId { get; }

        private ITextureResource CreateIcon()
        {
            TextureResource Map
            = new TextureResource("Map/FullMap");

            TextureResource Marker
            = new TextureResource("Map/Marker");

            BoundsUshort worldBounds = Api.Client.World.WorldBounds;

            Vector2D markerOffset = new Vector2D((position.X - worldBounds.Offset.X) * 391 / worldBounds.Size.X, 
                (position.Y - worldBounds.Offset.Y - worldBounds.Size.Y) * 352 / worldBounds.Size.Y);
            markerOffset += (-8, 15);

            return ClientProceduralTextureHelper.CreateComposedTexture(
                "Composed Teleporter Map",
                isTransparent: true,
                isUseCache: true,
                customSize: (391, 352),
                textureResourcesWithOffsets: new[]
                {
                    new TextureResourceWithOffset(Marker,
                                                  offset: markerOffset),
                    new TextureResourceWithOffset(Map,
                                                  offset: (0, 0)),
                });
        }
    }
}
