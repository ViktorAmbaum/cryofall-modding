﻿namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.WorldObjects.Teleporter
{
    using AtomicTorch.CBND.CoreMod.Helpers.Client;
    using AtomicTorch.CBND.CoreMod.Systems.VehicleGarageSystem;
    using AtomicTorch.CBND.CoreMod.Systems.VehicleSystem;
    using AtomicTorch.CBND.CoreMod.UI.Controls.Core;
    using AtomicTorch.CBND.CoreMod.Vehicles;
    using AtomicTorch.CBND.GameApi.Resources;
    using AtomicTorch.CBND.GameApi.Scripting;
    using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
    using AtomicTorch.GameEngine.Common.Extensions;

    public class ViewModelGarageVehicleEntry : BaseViewModel
    {
        private VehicleStatus status;

        public ViewModelGarageVehicleEntry(
            uint vehicleGameObjectId,
            IProtoVehicle protoVehicle,
            VehicleStatus status)
        {
            this.VehicleGameObjectId = vehicleGameObjectId;
            this.ProtoVehicle = protoVehicle;
            this.Status = status;
            TestIcon = CreateIcon();
        }

        public TextureBrush Icon => Api.Client.UI.GetTextureBrush(this.ProtoVehicle.Icon);

        private ITextureResource TestIcon;

        public TextureBrush MapIcon => Api.Client.UI.GetTextureBrush(TestIcon);

        public IProtoVehicle ProtoVehicle { get; }

        public VehicleStatus Status
        {
            get => this.status;
            set
            {
                if (this.status == value)
                {
                    return;
                }

                this.status = value;
                this.NotifyThisPropertyChanged();
                this.NotifyPropertyChanged(nameof(this.StatusText));
            }
        }

        public string StatusText => this.Status.GetDescription();

        public string Title => this.ProtoVehicle.Name;

        public uint VehicleGameObjectId { get; }

        private ITextureResource CreateIcon()
        {
            TextureResource TextureResourceMap
            = new TextureResource("StaticObjects/Structures/Misc/ObjectVehicleAssemblyBay_Platform");
            // size and all offsets are set in pixels
            return ClientProceduralTextureHelper.CreateComposedTexture(
                "Composed" + this.VehicleGameObjectId,
                isTransparent: true,
                isUseCache: true,
                customSize: (512, 572),
                textureResourcesWithOffsets: new[]
                {
                    new TextureResourceWithOffset(TextureResourceMap,
                                                  offset: (0, -180)),
                });
        }
    }
}