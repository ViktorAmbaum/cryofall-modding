Set-ExecutionPolicy RemoteSigned -Scope Process

Add-Type -A System.IO.Compression.FileSystem

# helper function to extract the compressed zip archive and replace it with an extracted folder under the same name
function Extract($FileName)
{
    if (![System.IO.File]::Exists($FileName))
    {
        return;        
    }

    echo "Extracting: $FileName"
    $tmp = $FileName + "_tmp"
    if ([System.IO.File]::Exists($tmp))
    {
        Remove-Item $tmp
    }

    [IO.Compression.ZipFile]::ExtractToDirectory($FileName, $tmp)
    Remove-Item $FileName
    Move-Item $tmp $FileName
}

# helper function to find all CPK and MPK files in the directory and extract them
function FindAndExtract($DirectoryPath)
{
    $files = Get-ChildItem $DirectoryPath -Include *.cpk, *.mpk
    ForEach ($file in $files)
    {
        Extract($file.FullName)
    }
}

FindAndExtract('.\*')
FindAndExtract('..\Data\Mods\*')