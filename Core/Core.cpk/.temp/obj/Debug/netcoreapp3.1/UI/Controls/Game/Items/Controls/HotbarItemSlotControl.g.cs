﻿#pragma checksum "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Items\Controls\HotbarItemSlotControl.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D6B9EF9DD7880D139CB75108C57DA8F702E101A1"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Controls;
using AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Data;
using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
using NoesisGUIExtensions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Controls {
    
    
    /// <summary>
    /// HotbarItemSlotControl
    /// </summary>
    public partial class HotbarItemSlotControl : AtomicTorch.GameEngine.Common.Client.MonoGame.UI.BaseUserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Items\Controls\HotbarItemSlotControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Controls.HotbarItemSlotControl UserControl;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Items\Controls\HotbarItemSlotControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Controls.ItemSlotControl ItemSlotControl;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Core;V1.0.0.0;component/ui/controls/game/items/controls/hotbaritemslotcontrol.xa" +
                    "ml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Items\Controls\HotbarItemSlotControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.UserControl = ((AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Controls.HotbarItemSlotControl)(target));
            return;
            case 2:
            this.ItemSlotControl = ((AtomicTorch.CBND.CoreMod.UI.Controls.Game.Items.Controls.ItemSlotControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

