﻿#pragma checksum "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Quests\QuestTracking\HUDQuestTrackingPanelControl.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2E2DF744A570C5A29BE53CE88DD9A373C5537DA1"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using AtomicTorch.GameEngine.Common.Client.MonoGame.UI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AtomicTorch.CBND.CoreMod.UI.Controls.Game.Quests.QuestTracking {
    
    
    /// <summary>
    /// HUDQuestTrackingPanelControl
    /// </summary>
    public partial class HUDQuestTrackingPanelControl : AtomicTorch.GameEngine.Common.Client.MonoGame.UI.BaseUserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 64 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Quests\QuestTracking\HUDQuestTrackingPanelControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl OuterBorder;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Quests\QuestTracking\HUDQuestTrackingPanelControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border InnerBorder;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Quests\QuestTracking\HUDQuestTrackingPanelControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel StackPanel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Core;V1.0.0.0;component/ui/controls/game/quests/questtracking/hudquesttrackingpa" +
                    "nelcontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\..\..\..\UI\Controls\Game\Quests\QuestTracking\HUDQuestTrackingPanelControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OuterBorder = ((System.Windows.Controls.ContentControl)(target));
            return;
            case 2:
            this.InnerBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 3:
            this.StackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

